<?php

namespace Drupal\quatre_d\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Element\EntityAutocomplete;

class ListeLivreController extends ControllerBase
{


  /**
   * Builds the response.
   */
  public function build($categorie) {

    $formValues = \Drupal::request()->query;

    $term = '';

    if (!empty($formValues->get('genre_litteraire'))){
      $term = $formValues->get('genre_litteraire');
    }

    if ($categorie = \Drupal::routeMatch()->getParameter('categorie')){
      $term = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties(['name' => $categorie]);
      $term = reset($term);
      $term = $term ? $term->id() : '';
    }


    $header = [
      'title'      => [
        'data'      => t('title'),
        'field'     => 'title',
        'specifier' => 'title',
      ],
      'date'       => [
        'data'      => t('Date de publication'),
        'field'     => 'field_date_de_publication',
        'specifier' => 'field_date_de_publication',
      ],
      'genre'       => [
        'data'      => t('Genre littéraire'),
        'field'     => 'field_genre_litteraire',
        'specifier' => 'field_genre_litteraire',
      ],
      'auteur'       => [
        'data'      => t('Auteur'),
        'field'     => 'field_auteur',
        'specifier' => 'field_auteur',
      ],
      'prix'       => [
        'data'      => t('Prix'),
        'field'     => 'field_prix',
        'specifier' => 'field_prix',
      ],
      'image'       => [
        'data'      => t('Image'),
        'field'     => 'field_image',
        'specifier' => 'field_image',
      ],
      'commentaires'       => [
      'data'      => t('Commentaires'),
      'field'     => 'field_commentaires',
      'specifier' => 'field_commentaires',
    ]
    ];

    $query = \Drupal::entityQuery('node')
      ->tableSort($header)
      ->condition('type', 'livres')
      ->condition('status', 1);
    if (!empty($term)){
      $query->condition('field_genre_litteraire', $term);
    }
    if (!empty($formValues->get('titre'))){
      $query->condition('title', '%'.$formValues->get('titre').'%','LIKE');
    }
    if (!empty($formValues->get('date'))){
      $query->condition('field_date_de_publication', $formValues->get('date'));
    }
    $nids = $query->pager(4)
      ->execute();

    $nids =  \Drupal\node\Entity\Node::loadMultiple($nids);

    $rows = [];
    foreach ($nids as $node){
      $row = [
        'data' => [
          $node->title->value,
          $node->field_date_de_publication->value,
          $node->field_genre_litteraire->entity->name->value,
          $node->field_auteur->entity->name->value,
          $node->field_prix->value,
          'image' => [
            'data' => [
              '#theme' => 'image_style',
              '#style_name' => 'thumbnail',
              '#uri' => $node->field_image->entity->uri->value,
            ]
          ],
          $node->field_commentaires->comment_count
        ]
      ];
      $rows[] = $row;
    }

    $render = [
      'table'           => [
        '#prefix'        => '<h1>Liste de Livres</h1>',
        '#theme'         => 'table',
        '#attributes'    => [
          'data-striping' => 0
        ],
        '#header' => $header,
        '#rows'   => $rows,
      ],
    ];


    return [
      '#theme' => 'page_liste_livres',
      '#table' => $render,
      '#form' => \Drupal::formBuilder()->getForm('Drupal\quatre_d\Form\LivresFiltreForm')
    ];

  }

}
