<?php

namespace Drupal\quatre_d\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;

/**
 * Provides a add form.
 */
class AjouterLivreForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quatre_d_ajouter_livre';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['quatre_d_titre'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Titre'),
      '#required' => TRUE,
    ];

    $form['quatre_d_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#required' => TRUE,
    ];

    $form['quatre_d_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Date de publication'),
      '#required' => TRUE,
    ];


    $form['quatre_d_genre_litteraire'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#title' => $this->t('Genre littéraire'),
      '#selection_settings' => [
        'target_bundles' => ['genre_litteraire'],
      ],
      '#required' => TRUE,
    ];

    $form['quatre_d_auteur'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Auteur'),
      '#target_type' => 'user',
      '#selection_handler' => 'default',
      '#required' => TRUE,
    ];

    $form['quatre_d_prix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prix'),
      '#required' => TRUE,
    ];

    $form['quatre_d_image'] = [
      '#type' => 'managed_file',
      '#title' => t('Image'),
      '#upload_validators' => array(
        'file_validate_extensions' => array('png jpg jpeg'),
        ),
      '#upload_location' => 'public://',
      '#required' => TRUE,
];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Enregistrer'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    /* create new node */
    $node = Node::create([
      'type'        => 'livres',
      'title'       => $values['quatre_d_titre'],
      'body'        => $values['quatre_d_description'],
      'field_auteur'        => $values['quatre_d_auteur'],
      'field_genre_litteraire'        => $values['quatre_d_genre_litteraire'],
      'field_prix'        => $values['quatre_d_prix'],
      'field_date_de_publication'        => $values['quatre_d_date'],
      'field_image' => [
        'target_id' => $values['quatre_d_image'][0],
        'alt' => $values['quatre_d_titre'],
      ],
    ]);

    /* save image permanently if node saved correctly */
    if ($node->save()){
      $file = File::load($values['quatre_d_image'][0]);
      $file->setPermanent();
      $file->save();
      \Drupal::messenger()->addStatus(t('Livre a été créé avec succès.'));
    }

  }

}
