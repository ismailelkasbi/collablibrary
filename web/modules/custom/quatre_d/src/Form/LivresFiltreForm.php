<?php

namespace Drupal\quatre_d\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a add form.
 */
class LivresFiltreForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quatre_d_livres_filtre';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $term = "";
    $params = \Drupal::request()->query;

    if ($params->get('genre_litteraire')){
      $term = \Drupal\taxonomy\Entity\Term::load($params->get('genre_litteraire'));
    }

    if ($categorie = \Drupal::routeMatch()->getParameter('categorie')){
      $term = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties(['name' => $categorie]);
      $term = reset($term);
    }

    $form['_titre'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Titre'),
      '#default_value' => $params->get('titre'),
      '#prefix' => '<div class="col-md-4">'
    ];

    $form['_genre_litteraire'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#title' => $this->t('Genre littéraire'),
      '#default_value' => $term ? $term : '',
      '#selection_settings' => [
        'target_bundles' => ['genre_litteraire'],
      ],
      '#suffix' => '</div>'
    ];

    $form['_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Date de publication'),
      '#default_value' => $params->get('date'),
      '#prefix' => '<div class="col-md-8">'
    ];


    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Recherche'),
      '#suffix' => '</div>'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $data = [
      'titre' => $form_state->getValue('_titre'),
      'genre_litteraire' => !empty($form_state->getValue('_genre_litteraire')) ? $form_state->getValue('_genre_litteraire') : '',
      'date' => $form_state->getValue('_date'),
    ];
    $path = \Drupal\Core\Url::fromRoute('quatre_d.liste_livres', $data)->toString();
    $response = new RedirectResponse($path);
    $response->send();
  }

}
