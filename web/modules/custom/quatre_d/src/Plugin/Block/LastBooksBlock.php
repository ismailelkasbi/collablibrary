<?php

namespace Drupal\quatre_d\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "quatre_d_last_book",
 *   admin_label = @Translation("Trois derniers livres"),
 *   category = @Translation("quatre_d")
 * )
 */
class LastBooksBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $query = \Drupal::entityQuery('node')
      ->condition('type', 'livres')
      ->condition('status', 1)
      ->sort('field_date_de_publication','DESC')
      ->range(0,3)
      ->execute();

    $data = \Drupal\node\Entity\Node::loadMultiple($query);

    $build['content'] = [
      '#theme' => 'block_last_books',
      '#data' => $data,
    ];

    return $build;
  }

}
