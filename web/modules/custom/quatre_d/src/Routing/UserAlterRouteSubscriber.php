<?php

namespace Drupal\quatre_d\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events and enables us to alter them.
 */
class UserAlterRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   * change user login path
   */
  protected function alterRoutes(RouteCollection $collection) {

    $user_login_route = $collection->get('user.login');
    $user_login_route->setPath('/console');

    $user_route = $collection->get('user.page');
    $user_route->setPath('/console');

  }

}
